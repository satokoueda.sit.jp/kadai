import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;


public class kadai_ver3 {
    private JPanel root;
    private JButton strawberryButton;
    private JButton puddingiceButton;
    private JButton chocoButton;
    private JButton chocomintButton;
    private JButton cidersorbetButton;
    private JButton checkOutButton;
    private JTextPane OrderedItems;
    private JTextPane Total_Price;
    private JButton cookyandcreamButton;

    int totalprice=0;

    public kadai_ver3() {
        strawberryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order_item("Caramel Ice Bar",300);
            }
        });
        strawberryButton.setIcon(new ImageIcon(this.getClass().getResource("caramel.png")));
        puddingiceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order_item("Pudding Ice Bar",280);
            }
        });
        puddingiceButton.setIcon(new ImageIcon(this.getClass().getResource("pudding.png")));

        chocoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order_item("Chocolate Ice Bar",320);
            }
        });
        chocoButton.setIcon(new ImageIcon(this.getClass().getResource("choco.png")));
        chocomintButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order_item("Chocolate Mint Ice Bar",350);
            }
        });
        chocomintButton.setIcon(new ImageIcon(this.getClass().getResource("chocomint.png")));
        cidersorbetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order_item("Cider Sorbet",250);
            }
        });
        cidersorbetButton.setIcon(new ImageIcon(this.getClass().getResource("cider.png")));

        cookyandcreamButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order_item("Cookies and Cream Ice Bar",330);
            }
        });
        cookyandcreamButton.setIcon(new ImageIcon(this.getClass().getResource("cookyandcream.png")));

        Total_Price.setText(totalprice+"yen");

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation =JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION
                );

                if(confirmation==0){
                    JOptionPane.showMessageDialog(null,
                            "The total price is "+totalprice+" yen.Thank you!");
                    OrderedItems.setText("");
                    totalprice=0;
                    Total_Price.setText("Total "+totalprice+" yen");
                }
            }
        });
        checkOutButton.setForeground(Color.RED);
    }

    void order_item(String food,int price) {
        int t=0;
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you put " + food + " in the basket?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if (confirmation == 0) {
            String currentText=OrderedItems.getText();
            OrderedItems.setText(currentText+food+" "+price+"yen\n");
            totalprice+=price;
            Total_Price.setText(totalprice+"yen");
            JOptionPane.showMessageDialog(null,"Thank you!");
        }

    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("kadai_ver3");
        frame.setContentPane(new kadai_ver3().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
